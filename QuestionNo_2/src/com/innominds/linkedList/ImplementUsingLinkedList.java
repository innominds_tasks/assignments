package com.innominds.linkedList;
// question no 1 implemented using linkedlist
import java.util.LinkedList;
import java.util.List;


public class ImplementUsingLinkedList {
	private LinkedList<Integer> list = new LinkedList<Integer>();

	private LinkedList<Integer> saveEvenNumbers(int n) {
		for (int i = 2; i <= n; i++) {
			if (i % 2 == 0) {
				list.add(i);
			}
		}
		return  list;

	}
	private LinkedList<Integer> printEvenNumbers() {
		LinkedList<Integer> a=new LinkedList<Integer>();
		
		for (int b : list) {
			a.add(b * 2);
			System.out.print(b*2+" ");
		}
		return a;		
	}
	private Integer printEvenNumber(int i) {
		if(list.contains(i)) {
			
			//return i;
			int index=list.indexOf(i);
			//return index;
			return list.get(index);
		}
		return 0;
        		
	}

	public static void main(String[] args) {
		ImplementUsingLinkedList data = new ImplementUsingLinkedList();
		data.saveEvenNumbers(20);
		data.printEvenNumbers();
		System.out.println(data.printEvenNumber(20));
        
	}
}
