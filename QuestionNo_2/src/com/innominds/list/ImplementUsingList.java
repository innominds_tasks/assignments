package com.innominds.list;

//question no 1 implemented using list
import java.util.ArrayList;
import java.util.List;

public class ImplementUsingList {

	private List<Integer> list = new ArrayList<Integer>();

	private List<Integer> saveEvenNumbers(int n) {
		for (int i = 2; i <= n; i++) {
			if (i % 2 == 0) {
				list.add(i);
			}
		}
		return list;

	}

	private List<Integer> printEvenNumbers() {
		List<Integer> a = new ArrayList<Integer>();

		for (int b : list) {
			a.add(b * 2);
			System.out.println(b*2+" ");
		}
		return a;
	}

	private Integer printEvenNumber(int i) {
		if (list.contains(i)) {

			// return i;
			int index = list.indexOf(i);
			 //return index;
			return list.get(index);
		}
		return 0;

	}

	public static void main(String[] args) {
		ImplementUsingList data = new ImplementUsingList();
		data.saveEvenNumbers(20);
		data.printEvenNumbers();
		System.out.println(data.printEvenNumber(20));
	}
}
