package com.innomids.question3;

public class EmployeeTest {

	public static void main(String[] args) {
		EmployeeDB empDB = new EmployeeDB();

		Employee emp1 = new Employee(1, "soham", "soham@gmail.com", 'M', 15000.0f);
		Employee emp2 = new Employee(2, "ram", "ram@gmail.com", 'M', 18000.0f);
		Employee emp3 = new Employee(3, "sham", "sham@gmail.com", 'M', 19000.0f);
		Employee emp4 = new Employee(4, "rushi", "rushi@gmail.com", 'M', 20000.0f);
		Employee emp5 = new Employee(5, "shubham", "shubham@gmail.com", 'M', 21000.0f);

		empDB.addEmployee(emp1);
		empDB.addEmployee(emp2);
		empDB.addEmployee(emp3);
		empDB.addEmployee(emp4);
		empDB.addEmployee(emp5);

		for (Employee emp : empDB.listAll())
			System.out.println(emp.GetEmployeeDetails());
		System.out.println();
		empDB.deleteEmployee(2);

		for (Employee emp : empDB.listAll())
			System.out.println(emp.GetEmployeeDetails());
		System.out.println();
		System.out.println(empDB.showPaySlip(3));

	}

}
