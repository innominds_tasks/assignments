package com.assignment.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import com.assignment.Employee;

public class EmployeeUtils {
	
	public static Employee RandomEmployeeForFreeToy(List<Employee> allEmployees) {

		Random random = new Random();

		Employee emp = new Employee();
		
		int randomIndex = random.nextInt(allEmployees.size());
		
		emp = allEmployees.get(randomIndex);

		return emp;
	}

	public static List<String> UniqueName(List<Employee> allEmployees) {
		List<String> list = new ArrayList<String>();

		allEmployees.forEach(employee -> {
			list.add(employee.getFirstName());
		});

		Set<String> set = new HashSet<String>();
		list.forEach(name -> {
			set.add(name);
		});

		List<String> uniqueFirstNames = new ArrayList<String>();
		uniqueFirstNames = set.stream().collect(Collectors.toList());

		return uniqueFirstNames;

	}

	public static Map<String, Integer> mostPopularNames(List<Employee> Employees) {

		Map<String, Integer> Map = new HashMap<String, Integer>();

		Employees.forEach(emp -> {
			if (Map.containsKey(emp.getFirstName())) {
				Map.put(emp.getFirstName(), Map.get(emp.getFirstName()) + 1);
			} else
				Map.put(emp.getFirstName(), 1);
		});

		return Map;
	}

	public static Queue<Employee> seasonTickets(List<Employee> allEmployees) {
		Queue<Employee> waitingList = new LinkedList<Employee>();
		allEmployees.forEach(emp -> {
			waitingList.add(emp);
		});
		return waitingList;

	}

	public static String addEmployeeToSeasonTicketList(List<Employee> allEmployees, Employee emp) {
		
		boolean result = seasonTickets(allEmployees).add(emp);

		if (result)

			return "employee Added";
		else

			return "Not Added";
	}

	public static String removeEmployeeFromSeasonTicketList(List<Employee> allEmployees, Employee emp) {
		boolean result = seasonTickets(allEmployees).remove(emp);

		if (result)

			return "employee removed";
		else
			return "Not Removed";
	}

	public static Employee findByEmpId(List<Employee> allEmployees, int empId) {
		for (Employee e : allEmployees) {
			if (e.getId() == empId)
				return e;
		}
		return null;
	}

}
