package com.assignment.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.assignment.Employee;
import com.assignment.utils.EmployeeUtils;

public class Application {

	public static void main(String[] args) {
		
		try (Scanner sc = new Scanner(System.in)) {

			List<Employee> allEmployees = new ArrayList<Employee>();
			allEmployees.add(new Employee(1, "Rohan", "Povhare", "rohan@test.com"));
			allEmployees.add(new Employee(2, "aditya", "shahapurkar", "adi@test.com"));
			allEmployees.add(new Employee(3, "Shubham", "kulkarni", "shubham@test.com"));
			allEmployees.add(new Employee(4, "manoj", "Jogdand", "mj@test.com"));
			allEmployees.add(new Employee(5, "rushi", "patil", "rushi@test.com"));
			allEmployees.add(new Employee(6, "rohit", "thorve", "rohit@test.com"));
			allEmployees.add(new Employee(7, "kedar", "khake", "kedar@test.com"));
			
			boolean exit = false;
			while (!exit) {

				System.out.println("Enter the choice");
				System.out.println("1. Choose any employee");
				System.out.println("2. Get List of Unique First Names");
				System.out.println("3. Count the number of Employees of Each Name");
				System.out.println("4. Add Employee Waiting Ticket List");
				System.out.println("5. Remove Employee from the Waiting Ticket List");
				System.out.println("6. Exit");

				int choice = sc.nextInt();
				switch (choice) {
				case 1:
					Employee emp = EmployeeUtils.RandomEmployeeForFreeToy(allEmployees);
					System.out.println(emp);
					break;

				case 2:
					List<String> uniqueNames = EmployeeUtils.UniqueName(allEmployees);
					System.out.println(uniqueNames);
					break;

				case 3:
					Map<String, Integer> mostPopularNames = EmployeeUtils.mostPopularNames(allEmployees);
					for (Map.Entry<String, Integer> entry : mostPopularNames.entrySet()) {
						System.out.println(
								"Name : " + entry.getKey() + " Number of Employees with This name : " + entry.getValue());
					}
					break;

				case 4:
					System.out.println("Enter empId, firstName, LastName, Email");
					Employee newEmployee = new Employee(sc.nextInt(), sc.next(), sc.next(), sc.next());
					System.out.println(EmployeeUtils.addEmployeeToSeasonTicketList(allEmployees, newEmployee));
					allEmployees.add(newEmployee);
					break;

				case 5:
					System.out.println("Enter empId of the employee to be removed : ");

					Employee emp1 = EmployeeUtils.findByEmpId(allEmployees, sc.nextInt());
					System.out.println(EmployeeUtils.removeEmployeeFromSeasonTicketList(allEmployees, emp1));
					allEmployees.remove(emp1);
					break;

				case 6:
					exit = true;
					break;
				}
			}
		}

	}

}
