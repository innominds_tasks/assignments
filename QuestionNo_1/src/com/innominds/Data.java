package com.innominds;
/*1. Develop a java class with a method saveEvenNumbers(int N) using ArrayList to store even numbers from 2 to N, where N is a integer which is passed as a parameter to the method saveEvenNumbers(). The method should return the ArrayList (A1) created.

In the same class create a method printEvenNumbers()which iterates through the arrayList A1 in step 1, and It should multiply each number with 2 and display it in format 4,8,12�.2*N. and add these numbers in a new ArrayList (A2). The new ArrayList (A2) created needs to be returned.

Create a method printEvenNumber(int N) parameter is a number N. This method should search the arrayList (A1) for the existence of the number �N� passed. If exists it should return the Number else return zero.

Hint: Use instance variable for storing the ArrayList A1 and A2.

NOTE: You can test the methods using a main method.*/
import java.util.ArrayList;

public class Data {

	private ArrayList<Integer> A1 = new ArrayList<Integer>();

	private ArrayList<Integer> saveEvenNumbers(int n) {
		//ArrayList<Integer> A2=new ArrayList<Integer>();
		for (int i = 2; i <= n; i++) {
			if (i % 2 == 0) {
			//	A2.add(i);
				A1.add(i);
			}
		}
		//System.out.println(A2);
		return  A1;

	}
	private ArrayList<Integer> printEvenNumbers() {
		ArrayList<Integer> A2=new ArrayList<Integer>();
		
		for (int b : A1) {
			A2.add(b * 2);
			System.out.print(b*2+" ");
		}
		return A2;		
	}
	private Integer printEvenNumber(int i) {
		if(A1.contains(i)) {
			
			//return i;
			int index=A1.indexOf(i);
			//return index;
			return A1.get(index);
		}
		return 0;
        		
	}
	public static void main(String[] args) {
		Data data = new Data();
		data.saveEvenNumbers(120);
		data.printEvenNumbers();
		
		System.out.println(data.printEvenNumber(2));
       
	}

}
